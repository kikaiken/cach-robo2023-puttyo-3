〇gitの使い方についてお願いなど
・最初にcloneした後に、CANlibを入れなおす必要が（多分）ある
　・入れ直した後に、cubeIDEがCANlibを読み込んでくれなかったら、一度ビルドするとよい
・作業するときは個人のブランチを作って作業してください
　・cloneした手元のフォルダーでgitbashを開いてコマンドを打つのがいいと思う
　　cf：https://qiita.com/TetsuTaka/items/5ab227a8bd2cd7106833
・作業に区切りがついたら、いきなりmainにpudhせずにmergeRequestを作成してください
　・mergeRequestは制御班の人が全員把握してから承認しましょう

〇作業する際に
主にmain.cのstartControll()の上にあるcontrollMCMDに追記することになると思う
動作確認用には別にsystemCheckTaskが存在しているので、活用してください

〇main.cについての説明
・usercode begin PVに諸々の構造体の定義
・main()の真上に、初期設定用の関数が、アクチュエータごとに存在
　・canSetting()で使う基盤の枚数を指定している
　・mcmdの設定は直動とターンテーブル用に二つあるので注意
usercode begin2にアクチュエータの初期設定を行う関数↑を実行するコードが存在
　・使わないアクチュエータは、関数実行部分をコメントアウトすること
startDefaultTask()はwikiの記事とほぼ同じ、触らない方が無難
startSystemCheck()とその上には、デバッグ用の機能が存在
　・個々のアクチュエータを別々に動作確認できる
　・使いたい関数のコメントアウトを解除すればよい
startControllerTask()はwikiの記事とほぼ同じ、触らない方が無難
　・コントローラーの値を全て表示させたいときは、UDPcontroller.cのprintControllerValueのコメントアウト解除
startControllTasl()とその上には本番用のコードが存在
　・各操作部分ごとの関数が用意されている
　　　・コントローラーの値を渡されているので、制御アルゴリズムの変更がしたければ、ここを変える
　・コントローラーの値を取得している。スティックは個別の変数に、ボタンはbutton_dataへ格納
　・mcmdの二つのエンコーダーの値とサーボの位置を格納する変数が存在
